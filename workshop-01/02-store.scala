//> using scala "2.13"
//> using lib "dev.zio::zio:2.0.0"
//> using lib "dev.zio::zio-streams:2.0.0"
//> using lib "dev.zio::zio-test:2.0.0"
//> using lib "software.amazon.awssdk:s3:2.17.233"

import zio._
import zio.stream._

trait Store {
  def ls: Task[List[String]]

  def isDirectory(name: String): Task[Boolean]

  def cd(name: String): Task[Unit]

  def mkdir(name: String): Task[Unit]
}

object Store {
  def ls: ZIO[Store, Throwable, List[String]] =
    ZIO.serviceWithZIO(_.ls)

  def isDirectory(name: String): RIO[Store, Boolean] =
    ZIO.serviceWithZIO(_.isDirectory(name))

  def cd(name: String): ZIO[Store, Throwable, Unit] =
    ZIO.serviceWithZIO(_.cd(name))

  def mkdir(name: String): ZIO[Store, Throwable, Unit] =
    ZIO.serviceWithZIO(_.mkdir(name))
}

/** Exercise 1. Create a layer that provides an implementation of the Store
  * trait that uses the S3 API.
  */

import software.amazon.awssdk.services.s3._
import software.amazon.awssdk.services.s3.model._

object Store1 extends ZIOAppDefault {
  val fromS3: ZLayer[Any, Throwable, Store] = ???

  val run = {
    def go: RIO[Store, Unit] =
      for {
        _ <- ZIO.logInfo("Listing current directory")
        names <- Store.ls
        _ <- ZIO.foreachDiscard(names)(f =>
          for {
            _ <- Console.printLine(s"  - $f")
            _ <- ZIO.ifZIO(Store.isDirectory(f))(Store.cd(f) *> go, ZIO.unit)
          } yield ()
        )
      } yield ()

    go.provide(fromS3)
  }
}

/** Exercise 2. S3 does not have the concept of directories. Create a layer that
  * provides an implementation of the Store trait that uses the S3 API but that
  * also has the concept of directories.
  */

object Store2 {
  val fromS3: ZLayer[Any, Throwable, Store] = ???

  val run =
    (
      for {
        _ <- ZIO.logInfo("Listing current directory")
        names <- Store.ls
        _ <- ZIO.foreachDiscard(names)(f => Console.printLine(s"  - $f"))
      } yield ()
    ).provide(fromS3)
}

/** Exercise 3. Imagine we need to use other AWS services. All APIs from the SDK
  * use an HTTP client, which by default is created on the fly. It would be such
  * a waste to have as many HTTP clients as we have layers.
  *
  * Create a layer that provides an implementation of the Store trait but
  * requires the http client to be provided by some other layer.
  */
